#ifndef POLYGON_H
#define POLYGON_H

#include <vector>

#include "vectorpoint.h"

class Polygon
{

private:
    VectorPoint m_normal;
    std::vector<VectorPoint> m_points;

public:
    Polygon();
    std::vector<VectorPoint> getPoints();
    VectorPoint getNormal();
    void setPoints(std::vector<VectorPoint> m_points);
    void setNormal(VectorPoint m_normal);

};

#endif // POLYGON_H
