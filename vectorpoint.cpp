#include "vectorpoint.h"

VectorPoint::VectorPoint(float m_x,float m_y,float m_z){
    this->m_x = m_x;
    this->m_y = m_y;
    this->m_z = m_z;
}

float VectorPoint::x(){
    return m_x;
}

float VectorPoint::y(){
    return m_y;
}

float VectorPoint::z(){
    return m_z;
}
