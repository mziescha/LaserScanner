###############################################################################
#
# run on console
# $> make clean; ~/Qt/5.11.1/gcc_64/bin/qmake Qt3DLaserScanner.pro && make && ./Qt3DLaserScanner
#
###############################################################################
#TEMPLATE = app
#TARGET = camera

QT += multimedia multimediawidgets

HEADERS = \
    camera.h \
    imagesettings.h \
    qlaser.h \
    stlfile.h \
    vectorpoint.h \
    polygon.h

SOURCES = \
    main.cpp \
    camera.cpp \
    imagesettings.cpp \
    qlaser.cpp \
    stlfile.cpp \
    vectorpoint.cpp \
    polygon.cpp

FORMS += camera.ui imagesettings.ui

RESOURCES += camera.qrc

INSTALLS += target

QT+=widgets

target.path = ./build
