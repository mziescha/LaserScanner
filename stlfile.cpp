#include "stlfile.h"
#include "polygon.h"

#include <cstring>
#include <fstream>


STLFile::STLFile() {}

void STLFile::write_stl(std::string filename, std::vector<Polygon> polygons){

    char head[80];
    std::ofstream myfile;
    char attribute[2] = "0";
    std::string header_info = "solid " + filename + "-output";
    std::strncpy(head,header_info.c_str(),sizeof(head)-1);
    unsigned long nTriLong = polygons.size() ;

    myfile.open((filename + "-out.stl").c_str(),  std::ios::out | std::ios::binary);
    myfile.write(head,sizeof(head));
    myfile.write(reinterpret_cast<char*>(&nTriLong), sizeof(nTriLong));

        //write down every triangle
    for (std::vector<Polygon>::iterator it = polygons.begin(); it!=polygons.end(); it++){
        //normal vector coordinates
        float point = it->getNormal().x();
        myfile.write(reinterpret_cast<char*>(&point), sizeof(point));
        point = it->getNormal().y();
        myfile.write(reinterpret_cast<char*>(&point), sizeof(point));
        point = it->getNormal().z();
        myfile.write(reinterpret_cast<char*>(&point), sizeof(point));

        myfile.write(attribute,2);
    }

    myfile.close();
}
