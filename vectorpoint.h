#ifndef VECTORPOINT_H
#define VECTORPOINT_H


class VectorPoint
{

private:
    float m_x;
    float m_y;
    float m_z;


public:
    VectorPoint();
    VectorPoint(float m_x,float m_y,float m_z);
    float x();
    float y();
    float z();

};

#endif // VECTORPOINT_H
