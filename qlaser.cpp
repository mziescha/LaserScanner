/****************************************************************************
**
****************************************************************************/

#include "qlaser.h"

#include <QImage>
#include <QTextStream>

QLaser::QLaser() : QObject()
{
    setMinHue(330);
    setMinSaturation(50);
    setMinValue(75);
}

QLaser::QLaser(int min_hue) : QObject()
{
    setMinHue(min_hue);
    setMinSaturation(50);
    setMinValue(75);
}

QLaser::QLaser(int min_hue, int min_saturation) : QObject()
{
    setMinHue(min_hue);
    setMinSaturation(min_saturation);
    setMinValue(75);
}

QLaser::QLaser(int min_hue, int min_saturation, int min_value) : QObject()
{
    setMinHue(min_hue);
    setMinSaturation(min_saturation);
    setMinValue(min_value);
}

std::vector<int> QLaser::minimize_thickness( std::vector<int> bwLine) {
    // manipulate image to get the laser
    int first_value = 0;
    int last_value = 0;
    QTextStream out(stdout);
    for (int i =0; i < bwLine.size()-1; i++){
        if (bwLine[i] == 1 && first_value == 0){
            first_value = i;
            last_value = i;
        }else if(bwLine[i] == 1){
            last_value = i;
        }
    }

    if (first_value == 0 && last_value == 0)
        return bwLine;

    int middle = (last_value - first_value) / 2;

    if (middle == 0)
        return bwLine;

    for (int i =0; i < bwLine.size()-1; i++){
        if ((first_value + middle) == i )
            bwLine[i] = 1;
        else
            bwLine[i] = 0;
    }

    return bwLine;
}

bool QLaser::is_catched_color( QColor color) {
    if ( color.hue() > this->getMinHue() && color.saturation() > this->getMinSaturation() && color.value() > this->getMinValue())
        return true;

    return false;
}

QImage QLaser::bwImage(const QImage originImage) {
    QImage bwImage(originImage.width(), originImage.height(), QImage::Format_RGB888);
    bwImage.fill(this->base_color);

    for(int ih = 0; ih < ( originImage.height()  - 1 ); ih++){
        std::vector<int> bwLine(originImage.width(), 0);
        for(int iw = 0; iw < ( originImage.width()  - 1 ); iw++){
            QColor color = originImage.pixelColor(iw, ih);
            if (is_catched_color(color))
                bwLine[iw] = 1;
        }

        bwLine = minimize_thickness(bwLine);
        for(int iw = 0; iw < ( originImage.width()  - 1 ); iw++)
            if(bwLine[iw] == 1)
                bwImage.setPixel(iw, ih, this->fill_color);
    }
    return bwImage;
}

void QLaser::convert_math_vector(QImage *bwImage){

}

void QLaser::setMinHue(int min_hue) {
     this->min_hue = min_hue;
}

void QLaser::setMinSaturation(int min_saturation) {
    this->min_saturation = min_saturation;
}

void QLaser::setMinValue(int min_value) {
    this->min_value = min_value;
}

int QLaser::getMinHue() {
     return this->min_hue;
}

int QLaser::getMinSaturation() {
    return this->min_saturation;
}

int QLaser::getMinValue() {
    return this->min_value;
}
