/****************************************************************************
**
****************************************************************************/

#ifndef CAMERA_H
#define CAMERA_H

#include "qlaser.h"

#include <QCamera>
#include <QCameraImageCapture>
#include <QMediaRecorder>
#include <QScopedPointer>

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Camera; }
QT_END_NAMESPACE

class Camera : public QMainWindow
{
    Q_OBJECT

public:
    Camera();

private slots:
    void setCamera(const QCameraInfo &cameraInfo);
    void startCamera();
    void stopCamera();
    void toggleLock();
    void takeImage();
    void displayCaptureError(int, QCameraImageCapture::Error, const QString &errorString);
    void configureCaptureSettings();
    void configureImageSettings();
    void displayCameraError();
    void updateCameraDevice(QAction *action);
    void updateCameraState(QCamera::State);
    void updateCaptureMode();
    void setExposureCompensation(int index);
    void setMinHue(int index);
    void setMinSaturation(int index);
    void setMinValue(int index);
    void processCapturedImage(int requestId, const QImage &img);
    void updateLockStatus(QCamera::LockStatus, QCamera::LockChangeReason);
    void displayViewfinder();
    void displayCapturedImage();
    void readyForCapture(bool ready);
    void imageSaved(int id, const QString &fileName);

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::Camera *ui;
    QLaser m_laser;
    QScopedPointer<QCamera> m_camera;
    QScopedPointer<QCameraImageCapture> m_imageCapture;
    QImageEncoderSettings m_imageSettings;
    bool m_isCapturingImage = false;
    bool m_applicationExiting = false;
};

#endif
