#ifndef STLFILE_H
#define STLFILE_H

#include "polygon.h"

#include <vector>
#include <string>

class STLFile
{
public:
    STLFile();
    void write_stl(std::string filename, std::vector<Polygon> polygons);
};

#endif // STLFILE_H
