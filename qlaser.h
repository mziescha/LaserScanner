/****************************************************************************
**
****************************************************************************/

#ifndef QLASER_H
#define QLASER_H

#include <QObject>
#include <QRgb>
#include <QColor>

#define PI 3.1415926535897932384626433

class QLaser : public QObject {

Q_OBJECT

public:
    explicit QLaser();
    explicit QLaser(int hue);
    explicit QLaser(int hue, int saturation);
    explicit QLaser(int hue, int saturation, int value);
    QImage bwImage(const QImage originImage);

// signals:

public slots:
    void setMinHue(int min_hue);
    void setMinSaturation(int min_saturation);
    void setMinValue(int min_value);
    int getMinHue();
    int getMinSaturation();
    int getMinValue();

private:
    int min_hue;
    int min_saturation;
    int min_value;
    QRgb base_color = QColor(Qt::black).rgb();
    QRgb fill_color = QColor(Qt::white).rgb();


private slots:
    std::vector<int> minimize_thickness( std::vector<int> bwLine);
    bool is_catched_color( QColor color);
    void convert_math_vector(QImage *bwImage);
};

#endif // QLASER_H
