
#include "polygon.h"

Polygon::Polygon(){}

std::vector<VectorPoint> Polygon::getPoints(){
    return m_points;
}
VectorPoint Polygon::getNormal(){
    return m_normal;
}
void Polygon::setPoints(std::vector<VectorPoint> m_points){
    this->m_points = m_points;
}
void Polygon::setNormal(VectorPoint m_normal){
    this->m_normal = m_normal;
}
